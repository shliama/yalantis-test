package my.shliama.yalantistest.activity;


import java.util.ArrayList;

import my.shliama.yalantistest.App;
import my.shliama.yalantistest.R;
import my.shliama.yalantistest.adapter.ListAdapter;
import my.shliama.yalantistest.model.NavigationDrawerListItem;
import my.shliama.yalantistest.model.NavigationDrawerListItemDetails;
import android.content.Context;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;


public class MainActivity extends BaseActivity {

    /* Views */
    private DrawerLayout          mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListView              mDrawerList;
    private ProgressBar           prBarConnectionStatus;
    private Button                btnTryAgain;
    private TextView              tvAddress;
    private TextView              tvContent;
    private ImageView             ivPicture;

    /* For title change */
    private CharSequence          mDrawerTitle;
    private CharSequence          mTitle;

    /* For drawer menu list */
    private ArrayList<NavigationDrawerListItem> navigationDrawerMenu;
    private ListAdapter         adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);


        tvAddress = (TextView) findViewById(R.id.details_tv_address);
        tvContent = (TextView) findViewById(R.id.details_tv_content);
        ivPicture = (ImageView) findViewById(R.id.details_iv_picture);
        prBarConnectionStatus = (ProgressBar) findViewById(R.id.details_pb_connection_status);
        btnTryAgain = (Button) findViewById(R.id.details_btn_try_again);

        initDrawer();
        getMenuList();
    }

    private void initDrawer() {

        navigationDrawerMenu = new ArrayList<NavigationDrawerListItem>();
        adapter = new ListAdapter(this, R.layout.menu_list_item, navigationDrawerMenu);

        mTitle = mDrawerTitle = getTitle();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList   = (ListView)     findViewById(R.id.left_drawer);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.drawable.ic_drawer,
                R.string.drawer_open,
                R.string.drawer_close
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                supportInvalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);
                supportInvalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private void getMenuList() {

        if (isInternetAvailable()) {
            App.apiManager.getItemsMenu();
        } else {
            btnTryAgain.setVisibility(View.VISIBLE);
        }
    }

    private boolean isInternetAvailable() {

        btnTryAgain.setVisibility(View.INVISIBLE);
        prBarConnectionStatus.setVisibility(View.VISIBLE);

        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            Toast.makeText(this, "Internet connection is not available", Toast.LENGTH_SHORT).show();
            prBarConnectionStatus.setVisibility(View.INVISIBLE);
            return false;
        }
    }

    /* Handling menuList clicks */

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {

        if (isInternetAvailable()) {
            NavigationDrawerListItem selectedItem = navigationDrawerMenu.get(position);

            App.apiManager.getItemDetails(String.valueOf(selectedItem.getId()));

            mDrawerList.setItemChecked(position, true);
            setTitle(selectedItem.getTitle());
            if ( this.mDrawerLayout.isDrawerOpen(this.mDrawerList)) {
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        }
    }

    /* Updating views with values from JSON requests */

    public void onEventMainThread(ArrayList<NavigationDrawerListItem> navigationDrawerMenu) {
        updateMenuList(navigationDrawerMenu);
    }

    private void updateMenuList(ArrayList<NavigationDrawerListItem> navigationDrawerMenu) {

        this.navigationDrawerMenu.addAll(navigationDrawerMenu);

        adapter.notifyDataSetChanged();

        prBarConnectionStatus.setVisibility(View.INVISIBLE);
    }

    public void onEventMainThread(NavigationDrawerListItemDetails itemDetails) {
        updateItemDetails(itemDetails.data.picture, itemDetails.data.adress, itemDetails.data.content);
    }

    private void updateItemDetails(String itemPicture, String itemAddress, String itemContent) {

        prBarConnectionStatus.setVisibility(View.INVISIBLE);

        tvAddress.setText(itemAddress);
        tvContent.setText(itemContent);
        Picasso.with(this).load(itemPicture).resize(200, 200).centerCrop().into(ivPicture);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.details_btn_try_again):
                getMenuList();
                break;
            default:
                break;
        }
    }

    /* Methods for navigation drawer */

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ( keyCode == KeyEvent.KEYCODE_MENU ) {
            if ( this.mDrawerLayout.isDrawerOpen(this.mDrawerList)) {
                this.mDrawerLayout.closeDrawer(this.mDrawerList);
            }
            else {
                this.mDrawerLayout.openDrawer(this.mDrawerList);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        if ( this.mDrawerLayout.isDrawerOpen(this.mDrawerList)) {
            this.mDrawerLayout.closeDrawer(this.mDrawerList);
        }
        super.onBackPressed();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mDrawerToggle.onOptionsItemSelected(item);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
}
