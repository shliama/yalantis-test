package my.shliama.yalantistest.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
	
	static final String dbName="YalantisTestDB";
	static final String itemsTable="items";
	static final String colID="ID";
	static final String colTitle="Title";
	static final String colBody="Body";
	static final String colPicture="Picture";

	public DBHelper(Context context) {
		super(context, dbName, null, 1);
			}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		  db.execSQL("CREATE TABLE " + itemsTable 
					  	+ "(" 
					  	+ colID + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
					  	+ colTitle + " TEXT, "
					  	+ colBody +" TEXT, "
					  	+ colPicture +" TEXT" 
					  	+ ");"
				    );
		  
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
				
	}

}
