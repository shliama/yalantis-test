package my.shliama.yalantistest.adapter;

import java.util.List;

import my.shliama.yalantistest.R;
import my.shliama.yalantistest.model.NavigationDrawerListItem;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class ListAdapter extends BaseListAdapter {

    private LayoutInflater inflater;
    
    
    public ListAdapter(Context context, int textViewResourceId, List<NavigationDrawerListItem> objects) {
        super(context, textViewResourceId, objects);
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NavigationDrawerListItem menuItem = this.getItem(position);
        return getItemView(convertView, parent, menuItem );
    }

    public View getItemView( View convertView, ViewGroup parentView, NavigationDrawerListItem navDrawerItem ) {

        NavigationDrawerListItem menuItem = navDrawerItem;
        NavMenuItemHolder itemHolder = null;

        if (convertView == null) {
            convertView = inflater.inflate( R.layout.menu_list_item, parentView, false);
            TextView titleView = (TextView) convertView
                    .findViewById( R.id.item_title );
            ImageView pictureView = (ImageView) convertView
                    .findViewById( R.id.item_picture );
            TextView bodyView = (TextView) convertView
                    .findViewById(R.id.item_body);

            itemHolder = new NavMenuItemHolder();
            itemHolder.titleView = titleView;
            itemHolder.iconView = pictureView;
            itemHolder.bodyView = bodyView;

            convertView.setTag(itemHolder);
        }

        if ( itemHolder == null ) {
            itemHolder = (NavMenuItemHolder) convertView.getTag();
        }

        itemHolder.bodyView.setText(menuItem.getBody());
        itemHolder.titleView.setText(menuItem.getTitle());
        Picasso.with(getContext()).load(menuItem.getPicture()).resize(70, 70).centerCrop().into(itemHolder.iconView);

        return convertView;
    }

    private static class NavMenuItemHolder {
        private TextView titleView;
        private ImageView iconView;
        private TextView bodyView;
    }

}
