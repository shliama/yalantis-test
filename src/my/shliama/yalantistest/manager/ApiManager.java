package my.shliama.yalantistest.manager;

import java.util.ArrayList;

import my.shliama.yalantistest.api.request.ItemDetails;
import my.shliama.yalantistest.api.request.ItemsMenu;
import my.shliama.yalantistest.model.NavigationDrawerList;
import my.shliama.yalantistest.model.NavigationDrawerListItem;
import my.shliama.yalantistest.model.NavigationDrawerListItemDetails;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.content.Context;
import de.greenrobot.event.EventBus;

public class ApiManager {
	
    /* URL to make request */
    public static final String PROTOCOL = "http://";
    public static final String DOMAIN_NAME = "apitest.yalantis.com";
    public static final String URL = PROTOCOL + DOMAIN_NAME;

    private RestAdapter restAdapter;
    private Context context;
    
    public ApiManager(Context context) {
    	
    	this.context = context;
    	
    	restAdapter = new RestAdapter.Builder()
        .setServer(URL)
        .build();
    }
	
    public void getItemsMenu() {

        ItemsMenu itemsList = restAdapter.create(ItemsMenu.class);

        itemsList.getItemsMenu(new Callback<NavigationDrawerList>() {

            @Override
            public void success(NavigationDrawerList itemsList, Response response) {

                ArrayList<NavigationDrawerListItem> navigationDrawerMenu = new ArrayList<NavigationDrawerListItem>();
                for (int i = 0; i < itemsList.items.size(); i++ ) {
                    navigationDrawerMenu.add(NavigationDrawerListItem.create(
                            itemsList.items.get(i).getId(),
                            itemsList.items.get(i).getTitle(),
                            itemsList.items.get(i).getBody(),
                            itemsList.items.get(i).getPicture(),
                            context
                    ));
                }

                EventBus.getDefault().post(navigationDrawerMenu);
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });

    }

    public void getItemDetails(String id) {

        ItemDetails itemDetails = restAdapter.create(ItemDetails.class);

        itemDetails.getItemDetails(id , new Callback<NavigationDrawerListItemDetails>() {

            @Override
            public void success(NavigationDrawerListItemDetails itemDetails, Response response) {

                EventBus.getDefault().post(itemDetails);
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });
    }

}
