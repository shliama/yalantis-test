package my.shliama.yalantistest.model;

import android.content.Context;

public class NavigationDrawerListItem {

    private int	 id;
    private String  picture;
    private String  title;
    private String  body;

    private NavigationDrawerListItem() {
    }

    public static NavigationDrawerListItem create( int id, String title, String body, String picture, Context context ) {
        NavigationDrawerListItem item = new NavigationDrawerListItem();
        item.setId(id);
        item.setTitle(title);
        item.setBody(body);
        item.setPicture(picture);
        return item;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
