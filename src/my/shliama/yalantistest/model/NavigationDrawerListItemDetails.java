package my.shliama.yalantistest.model;

public class NavigationDrawerListItemDetails {

    public ItemData data;

    public class ItemData {
        public String title;
        public String body;
        public String picture;
        public String adress;
        public String content;
        public int id;
    }
}
