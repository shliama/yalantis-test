package my.shliama.yalantistest.api.request;

import my.shliama.yalantistest.model.NavigationDrawerList;
import retrofit.Callback;
import retrofit.http.GET;

public interface ItemsMenu {
    @GET("/test/")
    void getItemsMenu(Callback<NavigationDrawerList> itemsMenuCallback);
}
