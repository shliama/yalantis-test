package my.shliama.yalantistest.adapter;

import java.util.List;

import my.shliama.yalantistest.model.NavigationDrawerListItem;
import android.content.Context;
import android.widget.ArrayAdapter;

public class BaseListAdapter extends ArrayAdapter<NavigationDrawerListItem> {

	public BaseListAdapter(Context context, int textViewResourceId, List<NavigationDrawerListItem> objects) {
		super(context, textViewResourceId, objects);
	}


}
