package my.shliama.yalantistest;

import my.shliama.yalantistest.manager.ApiManager;
import android.app.Application;

public class App extends Application {
	
	public static ApiManager apiManager;

    @Override
    public void onCreate() {
        super.onCreate();

        apiManager = new ApiManager(this);
    }

}
