package my.shliama.yalantistest.activity;

import android.support.v7.app.ActionBarActivity;
import de.greenrobot.event.EventBus;

public class BaseActivity extends ActionBarActivity {
	
    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

}
