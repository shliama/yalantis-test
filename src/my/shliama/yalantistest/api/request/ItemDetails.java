package my.shliama.yalantistest.api.request;

import my.shliama.yalantistest.model.NavigationDrawerListItemDetails;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

public interface ItemDetails {
    @GET("/test/")
    void getItemDetails(@Query("id") String id, Callback<NavigationDrawerListItemDetails> itemDetailsCallback);
}
